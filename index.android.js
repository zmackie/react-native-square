import React, { Component } from 'react'
import {
  ART,
  AppRegistry,
  StyleSheet,
  PanResponder,
  Animated,
  Dimensions,
  Image,
  Text,
  View
} from 'react-native'

import VectorWidget from './VectorWidget'
import * as Utils from './Utils'
import timer from 'react-native-timer'

const {
  Group,
  Shape,
  Surface,
} = ART;
const {HEIGHT, WIDTH} = Dimensions.get('window')
const SQUARE_DIMENSION = 200
const DOUBLE_TAP_DELAY = 300
const DOUBLE_TAP_RADIUS = 20

function doubleAlert() {
  return (
    <View style={styles.backgroundDropView}>
      <Text style={styles.headline}>TESTING</Text>
    </View>
  )
}

export default class react_art_demo extends Component {
  constructor() {
    super()
    this.state = {
      pan: new Animated.ValueXY(),
      showMsg: false,
      rotation: new Animated.Value(0)
    }
    this._AnimatedRotation = 0
    this._animatedValueX = 0
    this._animatedValueY = 0
    this.state.pan.x.addListener((value) => {
      this._animatedValueX = value.value
      console.log("x: ", this._animatedValueX)
    })
    this.state.pan.y.addListener((value) => {
      this._animatedValueY = value.value
      console.log("y: ", this._animatedValueY)
    })
    this._AnimatedRotation = this.state.rotation.interpolate({
      inputRange: [0, 360],
      outputRange: ['0deg', '360deg']
    })
    this.handlePanResponderGrant = this.handlePanResponderGrant.bind(this)
    this.handlePanResponderMove = this.handlePanResponderMove.bind(this)
    this._panResponder = PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onPanResponderGrant: this.handlePanResponderGrant,
      onMoveShouldSetResponderCapture: () => true,
      onMoveShouldSetPanResponderCapture: () => true,
      onPanResponderMove: this.handlePanResponderMove,
      onPanResponderRelease: () => {
        console.log('release')
        Animated.spring(this.state.pan, {toValue: {x:0, y:0}}).start()
      }
    })
    this.prevTouchInfo = {
      prevTouchX: 0,
      prevTouchY: 0,
      prevTouchTimeStamp: 0
    }
  }
  componentWillUnmount() {
    this.state.pan.x.removeAllListeners()
    this.state.pan.y.removeAllListeners()
    timer.clearTimeout(this)
  }
  getStyle() {
    var xyTranslate = this.state.pan.getTranslateTransform()
    var transforms = xyTranslate.concat({rotate: this._AnimatedRotation})
    console.log(xyTranslate)
    return [
      styles.square,
      { transform: transforms}
    ]
  }
  handlePanResponderMove(event, gestureState) {
    if (this.isRotate(event)){
      console.log('rotate!')
      let [pt1, pt2] = event.nativeEvent['changedTouches']
      let [t1, t2] = event.nativeEvent.touches
      let deg1 = Utils.angle(pt1.pageX, pt1.pageY, pt2.pageX, pt2.pageY)
      this.state.rotation.setValue(deg1)
    }
    this.state.pan.setValue({x: gestureState.dx, y: gestureState.dy})
  }
  isRotate(event) {
    return (event.nativeEvent.touches.length >= 2 && event.nativeEvent.changedTouches.length >= 2)
  }
  isDoubleTap(currentTouchTimeStamp, {x0, y0}) {
    var {prevTouchX, prevTouchY, prevTouchTimeStamp} = this.prevTouchInfo
    var dt = currentTouchTimeStamp - prevTouchTimeStamp
    return (dt < DOUBLE_TAP_DELAY && Utils.distance(prevTouchX, prevTouchY, x0, y0) < DOUBLE_TAP_RADIUS)
  }
  showMsg() {
    this.setState({showMsg: true}, () => timer.setTimeout(
      this, 'hideMsg', () => this.setState({showMsg: false}), 500
    ));
  }
  handlePanResponderGrant(e, gestureState) {
    var currentTouchTimeStamp = Date.now()
    if (this.isDoubleTap(currentTouchTimeStamp, gestureState)) {
      this.showMsg()
      console.log('Double tap!')
    }

    this.prevTouchInfo = {
      prevTouchX: gestureState.x0,
      prevTouchY: gestureState.y0,
      prevTouchTimeStamp: currentTouchTimeStamp
    }
  }
  render() {
    return (
      <View style={styles.container}>
      <Animated.View
        style={this.getStyle()}
        {...this._panResponder.panHandlers}>
          {this.state.showMsg ? doubleAlert() : null }
        </Animated.View>
      </View>
    )
  }

}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  square: {
    width: SQUARE_DIMENSION,
    height: SQUARE_DIMENSION,
    backgroundColor: 'blue'
  },
  backdrop: {
    flex: 1,

  },
  backgroundDropView: {
    width: WIDTH,
    height: HEIGHT,
    backgroundColor: 'rgba(0,0,0,0)'
  },
  headline: {
    fontSize: 40,
    textAlign: 'center',
    backgroundColor: 'rgba(0,10, 20, 0)',
    color: 'red'

  }
});
AppRegistry.registerComponent('react_art_demo', () => react_art_demo);
