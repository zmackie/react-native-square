export function distance(x0, y0, x1, y1) {
  return Math.sqrt( Math.pow((x1-x0), 2) + Math.pow((y1-y0), 2))
}


function toDeg (rad) {
  return rad * 180 / Math.PI
}

export function angle (touch1X, touch1Y, touch2X, touch2Y) {
  var deg = toDeg(Math.atan2( (touch2Y - touch1Y), (touch2X - touch1X)))

  if (deg < 0) {
    deg += 360
  }

  return deg
}
